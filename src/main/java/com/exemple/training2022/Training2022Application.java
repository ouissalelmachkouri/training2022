package com.exemple.training2022;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Training2022Application {

    public static void main(String[] args) {
        SpringApplication.run(Training2022Application.class, args);
    }

}
